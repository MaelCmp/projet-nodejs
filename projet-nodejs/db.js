const mongoose = require('mongoose');

const {
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB
} = process.env;

const options = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
};

//const url = `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;
const url = `mongodb://localhost/todo`;

mongoose.connect(url, options).then(function() {
  console.log("MongoDB est connecté");
}).catch(function(err) {
  console.log(err);
});
