// set date to datepicker
let dateObj = new Date();
// let day = dateObj.getDate();
let day = String(dateObj.getDate()).padStart(2, '0');
let month = ('0' + (dateObj.getMonth() + 1)).slice(-2);
let year = dateObj.getFullYear();

let date_now = year + "-" + month + "-" + day;
dateObj.setDate(dateObj.getDate() + 1);
let date_tomorrow = year + "-" + month + "-" + String(dateObj.getDate()).padStart(2, '0');

document.querySelector('input[name="date"]').setAttribute('min', date_now)

let element = document.querySelector('body');
if(! element.classList.contains('edit')) {
  document.querySelector('input[name="date"]').setAttribute('value', date_tomorrow)
}

function confirmSuppr(id){
  let confirmRes = confirm('Souhaitez-vous réellement supprimer cette tâche ?');
  if (confirmRes == true) {
    location.href = "/todo/delete/"+id;
  }
}

function todoCheck(id, checkbox) {
  location.href = "/todo/finish/" + id + "/" + checkbox.checked;
}

document.querySelectorAll('th:not(:last-child)').forEach(th => th.addEventListener('click', () =>{
  location.href = "/todo/sort/" + th.textContent;
}));
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJzY3JpcHQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBzZXQgZGF0ZSB0byBkYXRlcGlja2VyXG5sZXQgZGF0ZU9iaiA9IG5ldyBEYXRlKCk7XG4vLyBsZXQgZGF5ID0gZGF0ZU9iai5nZXREYXRlKCk7XG5sZXQgZGF5ID0gU3RyaW5nKGRhdGVPYmouZ2V0RGF0ZSgpKS5wYWRTdGFydCgyLCAnMCcpO1xubGV0IG1vbnRoID0gKCcwJyArIChkYXRlT2JqLmdldE1vbnRoKCkgKyAxKSkuc2xpY2UoLTIpO1xubGV0IHllYXIgPSBkYXRlT2JqLmdldEZ1bGxZZWFyKCk7XG5cbmxldCBkYXRlX25vdyA9IHllYXIgKyBcIi1cIiArIG1vbnRoICsgXCItXCIgKyBkYXk7XG5kYXRlT2JqLnNldERhdGUoZGF0ZU9iai5nZXREYXRlKCkgKyAxKTtcbmxldCBkYXRlX3RvbW9ycm93ID0geWVhciArIFwiLVwiICsgbW9udGggKyBcIi1cIiArIFN0cmluZyhkYXRlT2JqLmdldERhdGUoKSkucGFkU3RhcnQoMiwgJzAnKTtcblxuZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXRbbmFtZT1cImRhdGVcIl0nKS5zZXRBdHRyaWJ1dGUoJ21pbicsIGRhdGVfbm93KVxuXG5sZXQgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKTtcbmlmKCEgZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoJ2VkaXQnKSkge1xuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdpbnB1dFtuYW1lPVwiZGF0ZVwiXScpLnNldEF0dHJpYnV0ZSgndmFsdWUnLCBkYXRlX3RvbW9ycm93KVxufVxuXG5mdW5jdGlvbiBjb25maXJtU3VwcHIoaWQpe1xuICBsZXQgY29uZmlybVJlcyA9IGNvbmZpcm0oJ1NvdWhhaXRlei12b3VzIHLDqWVsbGVtZW50IHN1cHByaW1lciBjZXR0ZSB0w6JjaGUgPycpO1xuICBpZiAoY29uZmlybVJlcyA9PSB0cnVlKSB7XG4gICAgbG9jYXRpb24uaHJlZiA9IFwiL3RvZG8vZGVsZXRlL1wiK2lkO1xuICB9XG59XG5cbmZ1bmN0aW9uIHRvZG9DaGVjayhpZCwgY2hlY2tib3gpIHtcbiAgbG9jYXRpb24uaHJlZiA9IFwiL3RvZG8vZmluaXNoL1wiICsgaWQgKyBcIi9cIiArIGNoZWNrYm94LmNoZWNrZWQ7XG59XG5cbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ3RoOm5vdCg6bGFzdC1jaGlsZCknKS5mb3JFYWNoKHRoID0+IHRoLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgKCkgPT57XG4gIGxvY2F0aW9uLmhyZWYgPSBcIi90b2RvL3NvcnQvXCIgKyB0aC50ZXh0Q29udGVudDtcbn0pKTsiXX0=
