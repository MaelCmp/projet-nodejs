const path = require('path');
const Todo = require('../models/todo');
const { Parser } = require('json2csv');

exports.create = function (req, res) {
  if(req.body.name != '') {

    var name = new Todo({
      name: req.body.name,
      date: req.body.date,
      category: req.body.category
    });
    console.log(name);
    
    console.log(req.body);

    name.save(function (err) {
      if(err) {
        res.status(400).send('Unable to save todo to database');
      } else {
        res.redirect('/todo');
      }
    });

  }
};

function list(req, res, error) {
  Todo.find({}).exec(function (err, todo) {
    if (err) {
      return res.status(500).send(err);
    }

    res.render('todo', {
      todolist: todo,
      error: error
    });
  });
};

exports.list = list;

exports.edit = function(req, res) {
  if(req.method === "GET") {
    if(req.params.id !== '') {
      Todo.findById(req.params.id).exec(function (err, todo) {
        if(err) {
          list(req, res, 'Cet ID n\'existe pas');
        } else {
          res.render('edit',
          {
            todo: todo
          });
        }
      });
    }
  } else if(req.method === "POST") {
    if(req.body.name != '') {
      Todo.findByIdAndUpdate(req.params.id, { name: req.body.name, date: req.body.date, category: req.body.category }).exec(function(err, todo) {
        if(err) {
          return res.send(500, 'La mise à jour à échouée');
        } else {
          res.redirect('/todo');
        }
      });
    }
  }
  
};

exports.delete = function(req, res) {
  if(req.method === "GET") {
    if(req.params.id !== '') {
      Todo.findByIdAndRemove(req.params.id).exec(function(err, todo){
        res.redirect('/todo');
      });
    } else {
      list(req, res, 'Cet ID n\'existe pas');
    }
  }

};

exports.finish = function(req, res) {
  if(req.method === "GET") {
    if(req.params.id !== '') {
      Todo.findByIdAndUpdate(req.params.id, { finished: req.params.finish }).exec(function(err, todo) {
        res.redirect('/todo');
      });
    } else {
      list(req, res, 'Cet ID n\'existe pas');
    }
  }
};

exports.sort = function(req, res) {
  if(req.method === "GET") {
    if(req.params.col !== '') {
      switch (req.params.col) {
        case "Finis":
          Todo.find({}).sort({finished : 1}).exec(function (err, todo) {
            console.log(todo);
            if (err) {
              return res.send(500, err);
            }
        
            res.render('todo', {
              todolist: todo
            });
          });
          break;
        case "Nom":
            Todo.find({}).sort({name : 1}).exec(function (err, todo) {
              console.log(todo);
              if (err) {
                return res.send(500, err);
              }
          
              res.render('todo', {
                todolist: todo
              });
            });
          break;
        case "Date":
          Todo.find({}).sort({date : 1}).exec(function (err, todo) {
            console.log(todo);
            if (err) {
              return res.send(500, err);
            }
        
            res.render('todo', {
              todolist: todo
            });
          });
          break;
        case "Catégorie":
          Todo.find({}).sort({category : 1}).exec(function (err, todo) {
            console.log(todo);
            if (err) {
              return res.send(500, err);
            }
        
            res.render('todo', {
              todolist: todo
            });
          });
          break;
        default:
          res.redirect('/todo');
          break;
      }
    } else {
      list(req, res, 'Cette colonne n\'existe pas');
    }
  }
};

exports.export = function(req, res) {
  if(req.params.type === 'json') {
    Todo.find().lean().exec(function (err, todo) {
      res.writeHead(200, {'Content-Type': 'application/json','Content-disposition':`attachment; filename=todo-node.json`}); res.end(JSON.stringify(todo));
    });
  } else if(req.params.type === 'csv') {
    Todo.find().lean().exec(function (err, todo) {
      const json = JSON.stringify(todo);
      console.log(json);
      
      const fields = ['id', 'finished', 'name', 'date', 'category'];

      try {
        const json2csvParser = new Parser({ fields });
        const csv = json2csvParser.parse(json);
        console.log(csv);
        
        res.writeHead(200, {'Content-Type': 'text/csv','Content-disposition':`attachment; filename=todo-node.csv`}); res.end(csv);
      } catch (error) {
        console.log(error);
      }
    });
  }
};

exports.not_found = function(req, res) {
  res.render('404');
}
